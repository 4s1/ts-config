# Changelog

## 9.0.0 (2024-12-24)

Update to Node 22

## 8.1.0 (2024-04-07)

- remove `forceConsistentCasingInFileNames: true`, because it's the default value

## 8.0.0 (2024-03-22)

- remove `verbatimModuleSyntax: false` which is the default value

### Breaking Change

- enable `exactOptionalPropertyTypes`

#### How to fix

Mostly you have to add `undefined` to the property type and remove question mark.

```ts
// before
let foo?: string

// after
// remove question mark and add undefined
let foo: string | undefined
// or sometimes only add undefined
let foo?: string | undefined
```

Class properties can never be optional, they are always present.

```ts
// before
class Foo {
  // bar is always available, therefore question mark is not correct
  private bar?: string;
}

// after
class Foo {
  // Now bar is always available, but can contain the value "undefined"
  private bar: string | undefined;
}
```

## 7.0.0 (2023-10-31)

- Update readme
- Drop ts-node support

## 6.0.0 (2023-10-29)

- Remove Node.js 18 support

## 5.3.0 (2023-10-27)

- Add support for Node.js 20
- Update Node.js 18 configuration

## 5.2.1 (2023-07-25)

- Fix repo url

## 5.2.0 (2023-07-25)

- Migrate project from GitHub to Codeberg

## 5.1.0 (2023-06-30)

- chore: update `moduleResolution` to `bundler` in Node18 tsconfig

  before:

  ```json
  {
    "compilerOptions": {
      "moduleResolution": "node"
    }
  }
  ```

  after:

  ```json
  {
    "compilerOptions": {
      "moduleResolution": "bundler"
    }
  }
  ```

  For more details see:

  - https://www.typescriptlang.org/docs/handbook/module-resolution.html
  - https://github.com/tsconfig/bases/pull/197
  - https://github.com/tsconfig/bases/blob/main/bases/esm.json

## 5.0.1 (2023-06-15)

- Remove deprecated `tsconfig-es2020.json`

## 5.0.0 (2023-06-15)

- Recreate Node 18 configs
- Remove Node 20 config

## 4.3.2 (2023-05-01)

- Update documentation

## 4.3.1 (2023-05-01)

- publish merged configs instead of extending different configs to support `ts-node`, which doesn't support multiple extends attributes at the moment.

## 4.3.0 (2023-04-24)

- Add `tsconfig.json` to create sourcemaps and declaration files

## 4.2.0 (2023-04-24)

- Enhance Node `tsconfig` files
- Rewrite documentation

## 4.1.1 (2023-04-21)

- fix: add module attribute to Node 16 and Node 18 configs

## 4.1.0 (2023-04-21)

- Add Node 16 and Node 18 configs
- Rewrite React config

## 4.0.0 (2023-04-17)

### Breaking Change

- Enforce `override` keyword for overridden methods

## 3.0.2 (2023-03-30)

- Remove `exports` and `main` properties from `package.json`.
  Both paths pointed to a non-existent file.
  This doesn't work with TypeScript 5.0 (see [#53314](https://github.com/microsoft/TypeScript/issues/53314#issuecomment-1474186959))

## 3.0.1 (2023-01-17)

### Fixes

- Remove old text from readme

## 3.0.0 (2023-01-17)

### Breaking Changes

- This package is now pure ESM. Please [read this](https://gist.github.com/sindresorhus/a39789f98801d908bbc7ff3ecc99d99c).
- TS config `tsconfig-es2020-commonjs.json` for common js removed

## 2.1.0 (2022-09-22)

Enable `inlineSources` for all tsconfig presets.

## 2.0.0 (2022-08-05)

## 1.4.1 (2022-04-05)

## 1.4.0 (2021-12-15)

## 1.3.1 (2021-12-01)

## 1.3.0 (2021-11-09)

### Features

- add skipLibCheck

### Fixes

- remove duplicate file

## 1.2.2 (2021-10-06)

### Fixes

- set correct properties for react template

## 1.2.1 (2021-10-06)

### Fixes

- set correct properties for react template

## 1.2.0 (2021-10-06)

### Features

- add react config template
- rename ES2020 config template

## 1.1.0 (2021-10-01)

### Features

- add commonjs tsconfig

### Miscellaneous Chores

- update changelog-config

## 1.0.0 (2021-10-01)

### Documentation

- remove changelog to get a clear one
