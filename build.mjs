#!/usr/bin/env node

import strictest from "@tsconfig/strictest/tsconfig.json" with { type: "json" };
import node22 from "@tsconfig/node22/tsconfig.json" with { type: "json" };
import fs from "node:fs";

function write(filename, value) {
  fs.writeFileSync(filename, JSON.stringify(value, null, 2), { encoding: "utf-8" });
}

function createConfigForNode() {
  const defaultConfig = {
    $schema: "https://json.schemastore.org/tsconfig",
    compilerOptions: {
      ...strictest.compilerOptions,
      ...node22.compilerOptions,
    },
  };

  const nodeConfigWithSourcemaps = {
    ...defaultConfig,
    compilerOptions: {
      ...defaultConfig.compilerOptions,
      // Sourcemaps
      declaration: true,
      declarationMap: true,
      sourceMap: true,
      inlineSources: true,
    },
  };

  const nodeConfigWithoutSourcemaps = {
    ...defaultConfig,
    compilerOptions: {
      ...defaultConfig.compilerOptions,
    },
  };
  write("tsconfig-node.json", nodeConfigWithSourcemaps);
  write("tsconfig-node-wo-sourcemaps.json", nodeConfigWithoutSourcemaps);
}

createConfigForNode();
