# TS Config

This repository is used to centrally manage the base TypeScript configurations for all [4s1 organization](https://codeberg.org/4s1) projects.
All `tsconfig` files are based on recommendation from https://github.com/tsconfig/bases.

**Node 20**

- `@4s1/ts-config/tsconfig-node.json` extends
  1. [@tsconfig/strictest/tsconfig.json](https://github.com/tsconfig/bases/blob/main/bases/strictest.json)
  2. [@tsconfig/node20/tsconfig.json](https://github.com/tsconfig/bases/blob/main/bases/node20.json)
  3. ```json
     {
       "verbatimModuleSyntax": false,
       "exactOptionalPropertyTypes": false
     }
     ```
- `@4s1/ts-config/tsconfig-node-wo-sourcemaps.json` same as above plus
  ```json
  {
    "declaration": false,
    "declarationMap": false,
    "sourceMap": false,
    "inlineSources": false
  }
  ```

## Using

Install this package to get the configurations.

```bash
# npm
npm install @4s1/ts-config --save-dev
# pnpm
pnpm add @4s1/ts-config --save-dev
```

Create a `tsconfig.json` file and insert the following configuration.
If you do not want sourcemaps you can extend from `tsconfig-node-wo-sourcemaps.json`.

```json
{
  "$schema": "https://json.schemastore.org/tsconfig",
  "extends": "@4s1/ts-config/tsconfig-node.json",
  "compilerOptions": {
    "outDir": "./dist",
    "rootDir": "./src"
  },
  "include": ["src/**/*.ts"],
  "exclude": ["src/**/*.spec.ts", "node_modules/"]
}
```

Create a `tsconfig-dev.json` file and insert the following configuration.
This extends your previously created `tsconfig.json` and turns off some strict checking at development time.

```json
{
  "$schema": "https://json.schemastore.org/tsconfig",
  "extends": "./tsconfig.json",
  "compilerOptions": {
    "noUnusedLocals": false,
    "noUnusedParameters": false
  }
}
```

Finally build tasks for production and development have to be created.
Add the following scripts to your `package.json`.

```json
{
  "scripts": {
    "build": "rm -rf dist && tsc",
    "build:dev": "rm -rf dist && tsc --project tsconfig.dev.json"
  }
}
```

## How to update configs

```bash
# Create tsconfig files
./build.mjs
# Format tsconfig files
pnpm run format

# Compare changes
git diff

# Think about the changes

# Think about the changes again

# Commit what you want/need
```
